import Login from './modules/login/Login.vue';
import Index from './Dash.vue';

import Dashboard from './modules/dashboard/Dashboard.vue';
import Finance from './modules/finance/Finance.vue';
import Account from './modules/finance/account/Account.vue';
import Transaction from './modules/finance/transaction/Transaction.vue';

const routes = [
    {
      path: '/login',
      component: Login
    },
    {
      path: '/',
      component: Index,
      children: [
        {
            path: 'dashboard',
            alias: '',
            component: Dashboard,
            name: 'Dashboard'
        },
        {
            path: 'account',
            alias: '',
            component: Account,
            name: 'Account',
        },
        {
            path: 'transaction',
            alias: '',
            component: Transaction,
            name: 'Transaction'
        },
        {
            path: 'finance',
            alias: '',
            component: Finance,
            name: 'Finance'
        },
        
      ]
    }
  ]

export default routes;