import Vue from "vue";
import Vuex from "vuex";
 
Vue.use(Vuex);
 
export default new Vuex.Store({
    state: {
        user:null,
    },
    mutations: {
        changeState (state,payload) {
            state.user=payload;
        },
        resetState (state) {
            state.isLogin=false;
            state.token=null;
            state.user=null;
            state.lastLogin=null
        }
    },
    actions: {
        clearState ({ commit }) {
            commit('resetState');
        },
    }
});